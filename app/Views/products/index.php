<?php require_once('header.php'); ?>

<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Os melhores produtos do Brasil</h1>
            <p class="lead text-muted">Navegue em nossos produtos e se sinta a vontade no nosso catálogo. Entramos em contato em até 24 horas úteis após o envio do formulário de interesse de produtos. Os melhores preços e os melhores produtos!</p>
            <p>
                <a href="<?php echo url(''); ?>" class="btn btn-primary my-2">Home</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <?php
                    if (empty($products->count())) {
                        echo 'Sem produtos no momento.';
                    } else {
                        foreach ($products as $product) {
                ?>
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <a href="<?php echo url('product/' . $product->slug); ?>"><img class="card-img-top" src="<?php echo url('public/assets/images/' . $product->image); ?>" alt="<?php echo $product->name; ?>"></a>
                                <div class="card-body">
                                <p class="card-text"><?php echo $product->name; ?> </p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a href="<?php echo url('product/' . $product->slug); ?>"><button type="button" class="btn btn-sm btn-outline-secondary" title="Ver mais">Ver detalhes</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }} ?>
            </div>
        </div>
    </div>
</main>

<?php require_once('footer.php'); ?>