<?php require_once('header.php'); ?>

<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading"><?php echo $product->category; ?></h1>
            <p class="lead text-muted" style="font-size:20px;"><?php echo $product->name; ?></p>
            <p style="margin-top:12px;">
                <a href="<?php echo url('');?> " class="btn btn-primary my-2">Home</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12 artigo-box">
                    <center>
                        <img class="img-fluid" src="<?php echo url('public/assets/images/' . $product->image); ?>" style="margin-top:-20px;">
                        <p style="margin-bottom:20px;margin-top:8px;">Imagem do produto</p>
                    </center>

                    <?php echo $product->description; ?>

                    <h3 style="margin-top:25px;margin-bottom: 12px;">Tenho interesse</h3>

                    <form method="post" action="<?php echo url('product/sendInterest'); ?>">
                        <div class="form-group">
                            <label for="name">Nome *</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Preencha seu nome" required>
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Preencha seu E-mail">
                        </div>

                        <div class="form-group">
                            <label for="tel">Telefone de contato *</label>
                            <input type="text" class="form-control" id="tel" name="tel" required placeholder="Preencha seu telefone">
                        </div>

                        <input type="hidden" name="product_id" value="<?php echo $product->id; ?>" />

                        <button type="submit" style="margin-top:5px;" class="btn btn-primary">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<?php require_once('footer.php'); ?>