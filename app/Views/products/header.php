<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Projeto Ecommerce - Alcides Maya">
    <meta name="author" content="João Gustavo Balestrin dos Santos">
    <link rel="icon" href="<?php echo url('public/assets/images/favicon.png'); ?>">

    <title>Ecommerce - Catálogo de Produtos</title>

    <!-- Styles -->
    <link href="<?php echo url('public/assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo url('public/assets/css/fontawesome-all.css'); ?>">

    <!-- Custom styles -->
    <link href="<?php echo url('public/assets/css/style.css'); ?>" rel="stylesheet">
</head>
<body>
    <header>
        <div class="collapse bg-dark" id="navbarHeader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-7 py-4">
                        <h4 class="text-white">Sobre</h4>
                        <p class="text-muted">Ecommerce é um projeto criado para a prova G1/G2 no Alcides Maya. Foi fundado em 2018 pelo desenvolvedor e amante da tecnologia João Gustavo Santos. Contribua com nossos projetos open source também pelo <a href="https://github.com/jgustavo99" class="a-github" title="Acessar noso GitHub"target="_blank">GitHub.</a></a></p>
                    </div>
                    <div class="col-sm-4 offset-md-1 py-4">
                        <h4 class="text-white">Contate-nos</h4>
                        <ul class="list-unstyled">
                            <li><a href="https://twitter.com" class="text-white" target="_blank" title="Seguir no Twitter">Seguir no Twitter</a></li>
                            <li><a href="https://github.com/jgustavo99" class="text-white" target="_blank" title="Contribuir no GitHub">Contribuir no GitHub</a></li>
                            <li><a href="https://pt-br.facebook.com/espacodoprogramador" class="text-white" target="_blank" title="Curtir no Facebook">Curtir no Facebook</a></li>
                            <li><a href="mailto:contato@jgdesenvolvimento.com.br" class="text-white" target="_blank" title="Enviar um e-mail">Envie um e-mail</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between">
                <a href="<?php echo url(''); ?>" class="navbar-brand d-flex align-items-center">
                    <strong>Ecommerce</strong>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </header>