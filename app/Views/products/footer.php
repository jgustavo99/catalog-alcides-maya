<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Ir para o topo</a>
        </p>
        <p>&copy 2018 Catálogo de Produtos - Todos os direitos reservados</p>
    </div>
</footer>

<script src="<?php echo url('public/assets/js/jquery-slim.js'); ?>"></script>
<script src="<?php echo url('public/assets/js/bootstrap.min.js'); ?>"></script>
</body>
</html>