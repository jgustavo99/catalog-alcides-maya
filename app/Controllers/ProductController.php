<?php

namespace App\Controllers;

use App\Models\Product;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * ProductController
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2018
 * @package Catalog
 */
class ProductController
{
    /**
     * Retorna os produtos do catálogo
     *
     * @return mixed
     */
    public function index()
    {
        // Busca todos os produtos do catálogo
        $products = Product::all();

        // Retorna a view
        return view('products/index', ['products' => $products]);
    }

    /**
     * Retorna o produto encontrado do catálogo
     *
     * @param Request $request
     *
     * @return string|void
     */
    public function show(Request $request)
    {
        // Busca a slug do produto
    	$slug = $request->getAttribute('slug');

    	$product = Product::where('slug', $slug)->first();

    	if (empty($product)) {
    	    return 'Produto inexistente!';
        }

        // Retorna a view
        return view('products/show', ['product' => $product]);
    }

    /**
     * Envia o formulário de interesse de compra
     *
     * @param Request $request
     *
     * @return string|void
     */
    public function sendInterest(Request $request)
    {
        // Busca a slug do produto
        $data = $request->getParsedBody();
        $product = Product::find($data['product_id']);

        // Captura os dados do POST
        $name = $data['name'];
        $email = $data['email'];
        $tel = $data['tel'];
        $emailContact = 'joaogustavo.b@hotmail.com';
        $email_remetente = "sistema@php.com.br";
        $nameContact = 'João Gustavo';

        $body = "Olá! Foi enviado um formulário de interesse de compra no site. Segue os dados:";
        $body .= "<ul>";
        $body .= "<li>Produto: ". $product->name ."</li>";
        $body .= "<li>Nome: ". $data['name'] ."</li>";
        $body .= "<li>E-mail: ". (empty($data['email']) ? 'Não preenchido' : $data['email']) ."</li>";
        $body .= "<li>Telefone: ". $data['tel'] ."</li>";
        $body .= "</ul>";

        // MELHORIA - No futuro configurar o PHPMailer para enviar e-mail com autenticação SSL
        // mail() precisa estar com a porta 25 ativada
        $assunto = '=?UTF-8?B?'.base64_encode("Interesse de compra").'?=';
        $headers = "MIME-Version: 1.1\n";
        $headers .= "Content-type: text/plain; charset=iso-8859-1\n";
        $headers .= "From: $email_remetente\n";
        $headers .= "Return-Path: $email_remetente\n";
        $headers .= "Reply-To: $emailContact\n";
        $envio = @mail($emailContact, "Interesse de compra", $body, $headers, "-f$email_remetente");

        echo '<script>alert("Enviado com sucesso!");window.history.back();</script>';
    }
}
