<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Product - Eloquent Model
 *
 * @author João Gustavo Balestrin dos Santos <joaogustavo.b@hotmail.com>
 * @copyright (c) 2018
 * @package Catalog
 */
class Product extends Model
{
    /**
     * Tabela do banco de dados utilizada pela model
     *
     * @var string
     */
	protected $table = 'products';

    /**
     * Atributos assinaláveis em massa
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'image',
        'category'
    ];

    /**
     * Atributo das colunas de data
     *
     * @var array
     */
    protected $dates = [
        'created_at'
    ];
}
