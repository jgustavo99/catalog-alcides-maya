<?php

/**
 * BasicFrame - Framework Basic PHP
 * 
 * Rotas da aplicação com Slim - routes.php
 */

$app->get('/', 'App\Controllers\ProductController:index');

$app->get('/product/{slug}', 'App\Controllers\ProductController:show');

$app->post('/product/sendInterest', 'App\Controllers\ProductController:sendInterest');