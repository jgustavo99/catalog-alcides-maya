-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 02-Set-2018 às 23:07
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catalog-alcides`
--
CREATE DATABASE IF NOT EXISTS `catalog-alcides` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `catalog-alcides`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(220) NOT NULL,
  `slug` varchar(220) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(220) NOT NULL,
  `category` varchar(220) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `description`, `image`, `category`, `created_at`) VALUES
(1, 'Smartphone Samsung Galaxy S8 Plus Prata', 'smartphone-samsung-s8-plus-prata', 'Um smartphone revolucionário. Com o Galaxy S8+, tudo é ainda mais incrível. Desde a sua câmera de 12 MP até o seu sistema de reconhecimento de íris, que deixa o aparelho muito mais seguro. Sua tela infinita de 6.2 polegadas oferece uma experiência ainda mais imersiva, seja na hora de tirar fotos ou de assistir seus filmes e séries favoritos. E mais: se você tomar chuva, não se desespere, pois ele conta com resistência a respingos.', 'product_1.jpg', 'Telefonia', '2018-09-02 17:29:30'),
(2, 'Forno Elétrico Philco PFE38P 38L Preto 110V', 'forno-eletrico-philco-38l', 'Se você está procurando um eletrodoméstico moderno para dar aquele tornar as suas refeições mais rápidas e práticas, a melhor opção é o Forno Elétrico PFE38P 38L Preto 110V, da Philco. Além do amplo espaço interno, o produto tem como destaques a estrutura alongada que permite que você prepare uma gama maior de alimentos e a tampa de vidro para que você possa acompanhar de perto todo o alimento.', 'product_2.jpg', 'Cozinha', '2018-09-02 17:40:10'),
(3, 'Notebook 2 em 1 Lenovo Intel Core i7 8GB 1TB Windows 10 Tela 14\" Yoga 520 Prata\r\n', 'notebook-windows10-tela-14', 'O Notebook 2 em 1 Yoga 520 Prata, da Lenovo, é feito para te transformar em multi! Sua tela de 14¿, que gira 360 graus, permite diversos modos de uso. Equipado com a 7ª Geração de Processadores Intel, ele possui toda a performance necessária para trabalho e diversão, como 8GB de memória RAM, 1TB de armazenamento, sistema operacional Windows 10 e bateria com até 10 horas de uso.', 'product_3.jpg', 'Informática', '2018-09-02 17:38:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
